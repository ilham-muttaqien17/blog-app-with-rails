Feature: Manage Comments

    Background:
      Given I am on the login user page
      And fill the "Email" with "default@gmail.com"
      And fill the "Password" with "123123"
      When I click "Log in"
      Then I should see "Signed in successfully."
    
    Scenario: Create new comment
      Given I am on the post page
      And fill the "comment_body" with "My Comment"
      When I click "Reply" comment
      Then I should see the comment with value "My Comment"

    Scenario: Show comment
      Given I am on the post page
      When I click "Cek komentar"
      Then I should see the comment with value "My Comment"

    Scenario: Update comment
      Given I am on the post page
      And I click "Cek komentar"
      And I click "Update"
      And fill the "comment_body" with "Updated Comment"
      When I click "Update comment"
      Then I should see the comment with value "Updated Comment"

    Scenario: Delete comment
      Given I am on the post page
      And I click "Cek komentar"
      When I click "Delete"
      Then I shouldn't see the comment with value "Updated Comment"
