Feature: Manage Post

    Feature Description

    Background: Create New User
      Given the following user params:
        | email        | ilham@gmail.com                 |
        | password     | 123123                          |
        | confirmed_at | Wed, 21 Sep 2022 13:14:37 +0700 |
      Then I save user

      Given I am on the login user page
      And fill the "Email" with "default@gmail.com"
      And fill the "Password" with "123123"
      When I click "Log in"
      Then I should see "Signed in successfully."

    Scenario: Create new Post
      Given I am on the post page
      And I click "Create new post"
      And fill the "post_title" with "Test title"
      And fill the "post_body" with "Test desc"
      When I click "Create Post"
      Then I should see "Berhasil menambahkan postingan"
    
    Scenario: Show My Own Posts
      Given I am on the home page
      When I click "My Posts"
      Then I should see the post with following:
        | title | Test title |
        | body  | Test desc  |

    Scenario: Update post
      Given I am on the post page
      And I click "My Posts"
      And I click "Update"
      And fill the "post_title" with "Updated Title"
      And fill the "post_body" with "Updated Desc"
      When I click "Update Post"
      Then I should see "Berhasil memperbarui postingan"

    Scenario: Delete post
      Given I am on the post page
      And I click "My Posts"
      When I click "Delete"
      Then I should see "Berhasil menghapus postingan"