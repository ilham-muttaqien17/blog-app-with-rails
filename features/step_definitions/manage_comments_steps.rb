# User login
# Given(/^I am on the login user page$/) do
#   visit new_user_session_path
# end

# Given(/^I am on the post page$/) do
#   visit posts_path
# end

Then /^fill the "(.*?)" with "(.*?)"$/ do |field, value|
  if current_path == '/posts'
    find("#comment_body", match: :first).set(value)
  else
    fill_in field,	with: value 
  end
end

When(/^I click "(.*?)" comment$/) do |btn|
  find(".card-body", match: :first).click_on(btn)
end

Then(/^I should see the comment with value "(.*?)"$/) do |value|
  expect(page).to have_content(value)  
end

Then(/^I shouldn't see the comment with value "(.*?)"$/) do |value|
  expect(page).to have_no_content(value)  
end