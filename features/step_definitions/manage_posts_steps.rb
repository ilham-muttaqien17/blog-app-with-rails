
Given("the following user params:") do |params|
  email, password, confirmed_at = params.rows_hash.values_at(:email, :password, :confirmed_at)
  @user = User.new(email: email, password: password, confirmed_at: confirmed_at)
end

Then("I save user") do
  @user.save
end

# User login
Given(/^I am on the login user page$/) do
  visit new_user_session_path
end
# Given("the following post params:") do |params|
#   title, body, user_id = params.rows_hash.values_at(:title, :body, :user_id)
#   @post = Post.new(title: title, body: body, user_id: user_id)
# end

# When("I save post") do
#   @post.save
# end

# Then("I should have the following posts:") do |params|  
#   title, body = params.rows_hash.values_at(:title, :body)
#   @posts = Post.where("title LIKE ? AND body LIKE ?", "%#{title}%", "%#{body}%")
#   @posts.each do |post|
#     printf("| title | #{post.title} |")
#     puts
#     printf("| body  | #{post.body}  |")
#   end
# end

# Create new post
Given("I am on the post page") do
  visit posts_path
end

# Then /^fill the "(.*?)" with "(.*?)"$/ do |field, value|
#   fill_in field,	with: value 
# end

When(/^I click "(.*?)"$/) do |btn|
  if btn == "Update"
    find(".card-body", match: :first).click_on('Update')
  elsif btn == "Delete"
    # comment = Comment.last
    # delete "/comments/#{comment.id}"
    find(".card-body", match: :first).click_on('Delete')
    page.driver.browser.switch_to.alert.accept
  elsif btn == "Cek komentar"
    post = Post.last
    visit post_path(post.id)
  else
    click_on btn
  end
end

Then(/^I should see "(.*?)"$/) do |msg|
  expect(page).to have_content(msg)
end

# User own post
Given(/^I am on the home page$/) do
  visit root_path
end

When(/I click "(.*?)" on navigation bar$/) do |btn|
  click_link btn
end

Then(/^I should see the post with following:$/) do |params|
  title, body = params.rows_hash.values_at(:title, :body)
  expect(page).to have_content(title)
  expect(page).to have_content(body)  
end
